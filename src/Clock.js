import React from 'react'

export default class Clock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {date : new Date()}
    }
    
    componentDidMount(){
        this.timerID = setInterval(() => this.updateClock(), 2000);
    }
    componentWillMount(){
        clearInterval(this.timerID)
    }

    
    updateClock()
    {
        this.setState({
            date: new Date()
        })
    }

    render() {
        return (
            <div>
            {this.state.date.toLocaleTimeString()}        
            </div>
        )
    }
}
